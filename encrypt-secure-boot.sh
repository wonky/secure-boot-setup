#!/usr/bin/env bash

VERSION="0.6"

#################################################
# INSTALLATION CONFIGURATION
# target device
# !!!! be sure to get this right or you wipe the wrong device !!!!
# secure wipe target - meaning filling the device with meaningless characters
# recommended first time to obscure disk content
_secure_wipe="no"
_target_device="/dev/nvme0n1"
# encryption phrase
_crypt_phrase="86343"
_cryptsetup_options="--type luks2 --cipher aes-xts-plain --align-payload=8192 --key-size 256 --use-urandom"
# seconds to wait before terminating the encryption phrase prompt
_timeout=300
# hostname
_hostname="manjaro"
# bootloader
_boot_loader_label="Manjaro Verified Boot"
# -----------------------------------------------
# PARTITION CONFIGURATION VARIABLES
# partition order and size
_efi_part="1"
_efi_size="512M"
_swap_part="2"
_swap_size="16G"
_root_part="3"
_root_size="" # empty value will use remaining disk
# -----------------------------------------------
# SYSTEM LOCALE CONFIGURATION VARIABLES
_console_keymap="dk-latin1"
_console_font="lat2-16"
_console_font_map="8859-10"
_system_locale="en_DK.UTF-8"
_system_tz="Europe/Copenhagen"
_locale_gen_primary="en_DK.UTF-8 UTF-8"
_locale_gen_fallback="en_US.UTF-8 UTF-8"
_systemd_services="NetworkManager systemd-timesyncd systemd-resolved"
# -----------------------------------------------
# PACKAGES
_build_mirror="https://mirror.easyname.at/manjaro"
_base="base mc micro nano"
_kernel_version="6.10"
_kernel_pkg="linux610"
_packages="${_kernel_pkg} ${_kernel_pkg}-headers btrfs-progs efibootmgr git gptfdisk linux-firmware networkmanager plymouth polkit sbctl sudo wget systemd-resolvconf"
_branch="unstable" # requires plymouth 24.x
# installation mountpoint
_install_mount="/install"

# END CONFIGURATION
######################################################################
#
#                             ACHTUNG!
#                ALLES CODEMONKEYS UND DEVELOPERS!
# 
# Das codemachine ist nicht fuer gefingerpoken und mittengrappen.
# You might end up schnappen the crashtest, blowenfusen und debuggen
# with the headashbang.
# 
# Es ist nicht fuer gevurken by das dumkopfen. Das rubbernecken
# sightseeren, und das peering at this file without knowledge macht 
# bigge troubles und loss of sleepen. Das beste practice ist:
# keepen das fingers out of das unkaesslich code unless you know
# what you are doing.
#
# Relaxen und trusten das previouser coders, und if du must change,
# make sure you commitzen and testzen. If du breaken, fixen it schnell!
#
#                                         ~ The Code Elfen
#
######################################################################
# ensure root is used
if [[ $EUID -ne 0 ]]; then
    echo "Switch to root context e.g. 'su -l root' before executing this script."
fi

# default on a live media
# provided by package manjaro-tools-base
if ! command -v sgdisk &> /dev/null; then
    echo "Please ensure sgdisk from package gptfdisk is installed"
    exit 1
fi

IS_MANJARO=$(grep -e 'manjaro' /etc/os-release)
if [[ -z "${IS_MANJARO}" ]]; then
    echo "The script is written for Manjaro Linux - bailing out."
    exit 1
fi

LIBDIR="/usr/lib/manjaro-tools"
if [[ -f  "${LIBDIR}/util-msg.sh" ]]; then
    source "${LIBDIR}/util-msg.sh"
else
    echo "Required library not found"
    echo "Please sync package 'manjaro-tools-base-git'"
    exit 1
fi

clear
warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
warning "BIG FAT WARNING!"
warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
warning "This script will wipe the device: '${_target_device}'!"
warning "Please confirm by entering a capital 'y'. Any other key will exit script."
while true; do
       read -r -s -n 1 key
       case $key in
              Y) break
              ;;
              *) echo "Exit script"; exit 0
              ;;
       esac
done

clear
warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
warning "Double check ..."
warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
warning "Still want to wipe the device: '${_target_device}'?"
warning "Please enter a capital 'y'. Any other key will exit script."
while true; do
       read -r -s -n 1 key
       case $key in
              Y) break
              ;;
              *) echo "Exit script"; exit 0
              ;;
       esac
done

clear

_label_efi="EFI"
_label_crypt_swap="cryptswap"
_label_crypt_system="cryptsystem"
_label_swap="swap"
_label_system="system"
_cmdline="fbcon=nodefer rw rd.luks.allow-discards quiet bgrt_disable root=LABEL=${_label_system} rootflags=subvol=@root,rw  splash vt.global_cursor_default=0"
# timeout is time elapsed for accepting the decryption phrase
_crypttab_initramfs="${_label_system} /dev/disk/by-partlabel/${_label_crypt_system}  none timeout=120"

# create install mount point
if ! [[ -d ${_install_mount} ]]; then
    mkdir -p ${_install_mount}
fi

#################################################
# zero out _target_device
#################################################
info "Disk Setup"
msg "clearing partition info on ${_target_device}"
sgdisk --zap-all ${_target_device}
partprobe
udevadm settle

if [[ ${_secure_wipe} == "yes" ]]; then
    info "initializing disk"
    msg "create temporary luks container for randomization"
    # 
    cryptsetup -q open --type plain "${_target_device}" container --key-file /dev/urandom --cipher aes-xts-plain --key-size 256

    msg "randomizing container - please wait ..."
    # even though using /dev/zero
    # - since we are inside a luks containter
    # - zeroes are converted to random
    dd if=/dev/zero of=/dev/mapper/container status=progress bs=1M

    msg "closing temporary container"
    cryptsetup close container
    udevadm settle
fi


#################################################
# create partitions
#################################################

# msg "clearing exiting partition layout"
# sgdisk --clear  ${_target_device}
msg "create efi partition ${_efi_size}"
sgdisk -a 2 --new=1::${_efi_size}  --typecode=1:ef00 --change-name=1:${_label_efi} ${_target_device}
msg "create swap partition ${_swap_size}"
sgdisk -a 2 --new=2::${_swap_size} --typecode=2:8200 --change-name=2:${_label_crypt_swap} ${_target_device}
msg "create root partition"
sgdisk -a 2 --set-alignment=2048 --new=3::${_root_size} --typecode=3:8304 --change-name=3:${_label_crypt_system} ${_target_device}

msg "reloading partition table"
partprobe
udevadm settle

#################################################
# create and configure luks2 system container
#################################################
info "Setting up encryption"
_crypt_keyfile_tmp="/tmp/crypt_keyfile.bin"

if [[ -f ${_crypt_keyfile_tmp} ]]; then
    rm -f ${_crypt_keyfile_tmp}
fi

msg "generating temporary keyfile"
head -c 2048 /dev/urandom | od | tee ${_crypt_keyfile_tmp}

msg "using keyfile - creating new encryption container"
# cryptsetup 2.4.0 default 2024-02 - https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode
# cryptsetup --type luks2 --cipher aes-xts-plain64 --hash sha256 --iter-time 2000 --key-size 256 --pbkdf argon2id --use-urandom
cryptsetup luksFormat -q ${_cryptsetup_options} --pbkdf argon2id \
                      --key-file ${_crypt_keyfile_tmp}  \
                      /dev/disk/by-partlabel/${_label_crypt_system}

msg "using keyfile - opening container"
cryptsetup luksOpen /dev/disk/by-partlabel/${_label_crypt_system} ${_label_system} \
                    --key-file ${_crypt_keyfile_tmp}

msg "using keyfile - setting encryption phrase '${_crypt_phrase}'"
echo "${_crypt_phrase}" | cryptsetup -q luksAddKey --key-file ${_crypt_keyfile_tmp} /dev/disk/by-partlabel/${_label_crypt_system}

msg "removing temporary keyfile"
cryptsetup luksRemoveKey /dev/disk/by-partlabel/${_label_crypt_system} ${_crypt_keyfile_tmp}
udevadm settle


#################################################
# file system
# btrfs provides a better safeguard when it comes to data integrity
# this PoC will use a simple btrfs layout
#################################################
info "Filesystem setup"
msg "creating btrfs filesystem on '/dev/mapper/${_label_system}'"
mkfs.btrfs -f --label ${_label_system} /dev/mapper/${_label_system}
msg "temporary mount to '${_install_mount}'"
mount -t btrfs LABEL=${_label_system} ${_install_mount}
msg2 "creating @root"
btrfs subvolume create ${_install_mount}/@root
msg2 "creating @home"
btrfs subvolume create ${_install_mount}/@home
msg2 "creating @snapshots"
btrfs subvolume create ${_install_mount}/@snapshots

#################################################
# create and configure luks2 swap container
# WARNING
# using _swap_part makes hibernation impossible
#################################################
info "creating encrypted swap container"
cryptsetup open --type plain  --cipher aes-xts-plain --key-size 256 --key-file /dev/urandom /dev/disk/by-partlabel/${_label_crypt_swap} ${_label_swap}

msg "creating swap partition"
mkswap -L ${_label_swap} /dev/mapper/${_label_swap}

msg2 "activating encrypted swap"
swapon -L ${_label_swap}

info "umounting temporary mount"
umount -R ${_install_mount}
udevadm settle

#################################################
# remount root - remove ssd option if you are using a spinning disk
#################################################

_sv_def="rw,noatime,compress-force=zstd"
info "mounting @root"
mount -t btrfs -o ${sv_def},x-mount.mkdir,subvol=@root LABEL=${_label_system} ${_install_mount}

info "mounting @home"
mount -t btrfs -o ${sv_def},x-mount.mkdir,subvol=@home LABEL=${_label_system} ${_install_mount}/home

info "mounting @.snapshots"
mount -t btrfs -o ${sv_def},x-mount.mkdir,subvol=@snapshots LABEL=${_label_system} ${_install_mount}/.snapshots

udevadm settle

#################################################
# EFI system partition
#################################################
info "creating $esp"
mkfs.fat -F32 -n $_label_efi /dev/disk/by-partlabel/$_label_efi

info "format esp"
mkdir -p ${_install_mount}/efi

info "mount esp"
mount LABEL=$_label_efi ${_install_mount}/efi
udevadm settle

#################################################
# Bootstrapping base system
#################################################
info "Setting build mirror and branch"
pacman-mirrors --api --url ${_build_mirror} --set-branch ${_branch}

info "Getting cpu info"
_cpu=$(cat /proc/cpuinfo | grep 'vendor_id' | awk 'NR==1 {print $3}')
if [[ ${_cpu} =~ "AuthenticAMD" ]]; then
    _ucode="amd-ucode"
else
    _ucode="intel-ucode"
fi

info "bootstrapping base ${_ucode}"

basestrap ${_install_mount} ${_base} ${_ucode}

#################################################
# Configure base
#################################################
# generate fstab with labels
info "generate fstab"
fstabgen -L -p ${_install_mount} >> ${_install_mount}/etc/fstab

info "System Configuration tasks"

# configure keymap and console font
msg "creating configuration files"
msg2 "setting console keymap"
echo "KEYMAP=${_console_keymap}" > ${_install_mount}/etc/vconsole.conf

msg2 "setting console font"
echo "FONT=${_console_font}" >> ${_install_mount}/etc/vconsole.conf

msg2 "setting console font map"
echo "FONT_MAP=${_console_font_map}" >> ${_install_mount}/etc/vconsole.conf

msg2 "setting system locale"
echo "LANG=${_system_locale}" > ${_install_mount}/etc/locale.conf

msg "Setting target mirror and branch"
pacman-mirrors --api --prefix ${_install_mount} --url ${_build_mirror} --set-branch ${_branch}

info "Writing script for second step - chroot setup"

#####################################################################
# THIS PART CREATES THE SCRIPT TO BE EXECUTED IN THE CHROOT
#
cat << EOF > ${_install_mount}/final-script
#####################################################################
# this file is to be launched manually after entering chroot environment
#####################################################################
_boot_loader_label='${_boot_loader_label}'
_branch='${_branch}'
_build_mirror='${_build_mirror}'
_cmdline='${_cmdline}'
_console_font='${_console_font}'
_console_font_map='${_console_font_map}'
_console_keymap='${_console_keymap}'
_crypttab_initramfs='${_crypttab_initramfs}'
_efi_part='${_efi_part}'
_groups='${_groups}'
_install_mount='${_install_mount}'
_kernel_version='${_kernel_version}'
_label_crypt_swap='${_label_crypt_swap}'
_label_crypt_system='${_label_crypt_system}'
_label_swap='${_label_swap}'
_label_system='${_label_system}'
_locale_gen_fallback='${_locale_gen_fallback}'
_locale_gen_primary='${_locale_gen_primary}'
_packages='${_packages}'
_shell='${_shell}'
_system_locale='${_system_locale}'
_system_tz='${_system_tz}'
_systemd_services='${_systemd_services}'
_target_device='${_target_device}'

msg "Installing target packages"
pacman -S --noconfirm ${_packages}

msg "update installed swap config"

# ensure swap is mounted on the encrypted swap
msg2 "pointing fstab to encrypted swap"
sed -i 's|LABEL='\${_label_swap}'|/dev/mapper/'\${_label_swap}'|g' /etc/fstab

# swap is not persistent across boot - no hibernation
msg2 "adding swap to crypttab"
echo "\${_label_swap} /dev/disk/by-partlabel/\${_label_crypt_swap} /dev/urandom swap,offset=2048,cipher=aes-xts-plain64,size=256" >> /etc/crypttab

# write final kernel commandline
msg "writing kernel command line"
echo \${_cmdline} > /etc/kernel/cmdline

# configure locale
msg "configure locale generation"
echo "#" >> /etc/locale.gen
echo "# script configured locale" >> /etc/locale.gen
echo \${_locale_gen_primary} >> /etc/locale.gen
echo \${_locale_gen_fallback} >> /etc/locale.gen

# generate locales
msg2 "generate locale"
locale-gen

# timezone
msg "setting timezone"
ln -sf "/usr/share/zoneinfo/\${_system_tz}" /etc/localtime

# hardware clock
msg "setting clock"
hwclock --systohc --utc

# enable wheel group
msg "enable wheel"
echo '%wheel ALL=(ALL) ALL' > /etc/sudoers.d/10-installer

# enable services
msg "enable services"
systemctl enable \${_systemd_services}

#################################################
# chroot configure secure boot
#################################################
info "configure verified boot setup"

# configure hooks
msg "configure mkinitcpio"
sed -i 's|HOOKS=.*|HOOKS=(systemd plymouth kms modconf keyboard block filesystems btrfs sd-vconsole sd-encrypt fsck)|g' /etc/mkinitcpio.conf

# create crypttab.initramfs
msg "configure crypttab initramfs"
echo \${_crypttab_initramfs} > /etc/crypttab.initramfs

# This step is Manjaro specific due to different kernel name schema
# create symlinks for sbctl to recognize init
msg "creating symlinks for sbctl using kernel \${_kernel_version}"
cd /boot
ln -s initramfs-\${_kernel_version}-x86_64.img initramfs-linux.img
ln -s vmlinuz-\${_kernel_version}-x86_64 vmlinuz-linux
cd /

msg "setting Plymouth theme"
plymouth-set-default-theme -R spinner

# create keys
msg "generating verified boot keys"
sbctl create-keys

# generate unified kernel image bundle
msg "generate efi image bundle"
ESP_PATH='/efi' sbctl bundle -s '/efi/main.efi'

# create efi entry
msg "create EFI boot entry"
efibootmgr  --loader main.efi --create --disk ${_target_device} --part ${_efi_part} --label '${_boot_loader_label}' --unicode

clear
warning "#############################################################"
warning " POST BOOT SETUP                                           #"
warning "  Reboot the _system into UEFI setup.                      #"
warning "  Enable Setup Mode in secure boot settings.               #"
warning "  MAKE SURE you understand the implications of this,       #"
warning "  and check to make sure it is safe for your device.       #"
warning "  Some laptops have been bricked erasing the default keys. #"
warning "  Once booted, login and enroll the secure boot keys.      #"
warning " --------------------------------------------------------- #"
warning " THIS CAN BRICK YOUR DEVICE.                               #"
warning "  On some systems, OpROMs                                  #"
warning "  (Option ROMs, essentially UEFI-level device drivers)     #"
warning "  may be present and required,                             #"
warning "  which have to be signed with secure boot to work.        #"
warning "                                                           #"
warning "  If you do this improperly, the device WILL BE BRICKED.   #"
warning "  For more information, see man page for sbctl.            #"
warning " <https://github.com/Foxboron/sbctl/wiki/FAQ#option-rom>   #"
warning "############################################################"
warning "Confirm by entering 'y'"
while true; do
       read -r -s -n 1 key
       case \$key in
              y|Y) break
              ;;
              *)
              ;;
       esac
done

clear
# create root password
msg "Create root password"
passwd
warning "Type exit followed by Enter to exit chroot"
warning "Reboot your system to test the installation"
warning "After reboot login as root"

info "First ensure bundles are generated and signed"
msg "sbctl generate-bundles -s"

info "Then enroll keys into the Secure Boot"
msg "sbctl enroll-keys"

warning "For production be sure to protect the firmware against unauthorized changes"

info "TODO - remove stage2.sh script"
#rm -f /step2.sh

EOF

#
# END step2.sh
#####################################################################


# concatenate util-msg.sh with finale-script
# stage2.sh
cat /usr/lib/manjaro-tools/util-msg.sh ${_install_mount}/final-script > ${_install_mount}/stage2.sh
rm -f ${_install_mount}/final-script

info "Entering chroot"
msg "Execute stage 2 'bash stage2.sh'"

# chroot
manjaro-chroot ${_install_mount} /bin/bash
