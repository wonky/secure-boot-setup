linux-aarhus | 2024-02-14 16:23:30 UTC | #1

<div data-theme-toc="true"> </div>

**Difficulty: ★★★★★**

<big>Work In Progress</big>

## Verified Boot
Please note the distrinction **Verified** vs. **Secure**
* Please respect that this topic is **NOT** a discussion of pro's and con's of Secure Boot.
* Constructive feedback - errors, typos, improvements are welcome. 
* Opinions on Microsoft, the boot method or TPM has been discussed ad nauseum. 
* Been there done that - time to move on :motorcycle: .

## Inspiration and credit
Accidently stumbled on this [[User:ZachHilman/Installation_-_Btrfs_%2B_LUKS2_%2B_Secure_Boot]] and decided to get my feet wet and my hands dirty trying to implement encrypted Manjaro Linux using Verified Boot.

## Proof of Concept
Proof of Concept project includes all what previously has been next to possible to achieve with Manjaro LInux

- no reliance on Microsoft (unless your system requires signed optional-rom image(s)
- no boot loader - the system loads directly using a signed kernel image
- full disk encryption using luks2 with argon2id
- unified kernel image
- directly signed using a custom certificate
- signing key enrolled in the firmware

This is a primer into creating a Manjaro Linux system using Verified Boot.

There is no bootloader involved - the system is booted using EFI and a unified kernel image.

* :warning: If you use this in production - be sure to password protect your firmware.
* :warning: When it comes to boot method and the use of sbctl the installation is unsupported by Manjaro Linux.
* :dragon: Due to the use of **sbctl** sym-links is used to point to the kernel.
* :dragon: If you install another kernel - you must manually update these sym-links. 
* :dragon: The sym-links is found in /boot
* :information_source: The installed system is otherwise fully supported.
* :information_source: Should you need help otherwise be sure to mention you installed using this topic.

The script is constantly changing - the first stable version is tagged v0.1-stable

https://codeberg.org/wonky/secure-boot-setup/releases

## Important reading
Be sure to read https://github.com/foxboron/sbctl/wiki/FAQ#option-rom and ensure your system is not using any signed option-rom images. **IF** you do not acknowledge this you may brick your machine. Don't be scared - as a safe guard you have the option of enrolling factory certificates.

## Hardware tested
These systems do not implement any signed option-rom images

- Clevo N141WU
- Tuxedo InfinityBook Pro 14 Gen8

### Requirements
A Manjaro ISO and a capable system.

Technically this is a two part operation.

When the initial setup has been done the script writes out a second script and drop you into a chroot where you excute stage 2.

### The steps
1. Enter the system firmware
2. Enable Secure Boot
3. Ensure Secure Boot is in setup mode
5. Disable factory key enrollment
4. Save and reboot using a Manjaro Linux ISO
5. Connect to network
6. Switch the context to root
   ```
   su
   ```
7. Clone the script
   ```
   git clone https://codeberg.org/wonky/secure-boot-setup
   cd secure-boot-setup
   ```
8. Edit the script
   - see below - especially the target device
9. Save the script and execute step 1 (one)
    ```
    bash encrypt-secure-boot.sh
    ```
10. When part1 has finished you dumped into a chroot and you must manually execute step 2 (two)
    ```
    bash step2.sh
    ```

### Post setup
Due to limitations in chroot - it is not always possible to enroll the generated keys into the firmware.

To continue with this last step reboot the system and login as root - this provides an excellent opportunity to test your installation before activating Secure Boot.

When logged in as root
```
sbctl generate-bundles -s
sbctl enroll-keys
```

### Install a stock Manjaro desktop
Now that you have a very, very basic bootable system, you would probably want a GUI.

Please note that not all packages, normally installed for a Manjaro root filesystem has been installed.

**mapare** is script created by @cscs for a completely different task but we will abuse it to create a stock Manjaro desktop (You can make the **mapare** script install all packages by changing the **-D** argument to **-A** however it is untested for this usecase)
```
git clone https://gitlab.com/cscs/mapare.git
cd mapare
bash mapare -I -D
```
When using the -D you will need to manually add **manjaro-zsh-config** to get a functional .zshrs

Copy the content of /etc/skel to the home of the user you created using the initial script
```
cp -R /etc/skel/.* /home/<username>
```

You will also need to set the keyboard to be used with the desktop - if you cannot remember your chosen layout you can get it using 
```
localectl status
```

Now set the locale using the VC Keymap value
```
localectl set-keymap <keymap>
```

When completed you need to enable the SDDM service
```
systemctl enable sddm
```

Now the basic setup has been done and it is time for restart
```
reboot
```

## Customize the script
Although the script requires some interaction it is written with the intent of automating as much of the process as possible.
The script will use the default format command as described at https://wiki.archlinux.org/title/Dm-crypt/Device_encryption#Encryption_options_for_LUKS_mode

The script configuration is in the top most part of the script and the variable names will tell you what it is used for.

The following variables will define your installation.
```
#################################################
# INSTALLATION CONFIGURATION
# target device
# !!!! be sure to get this right or you wipe the wrong device !!!!
# secure wipe target - meaning filling the device with meaningless characters
# recommended first time to obscure disk content
_secure_wipe="no"
_target_device="/dev/nvme0n1"
# encryption phrase
_crypt_phrase="86343"
_cryptsetup_options="--type luks2 --cipher aes-xts-plain --align-payload=8192 --key-size 256 --use-urandom"
# seconds to wait before terminating the encryption phrase prompt
_timeout=300
# hostname
_hostname="manjaro"
# bootloader
_boot_loader_label="Manjaro Verified Boot"
# -----------------------------------------------
# PARTITION CONFIGURATION VARIABLES
# partition order and size
_efi_part="1"
_efi_size="512M"
_swap_part="2"
_swap_size="16G"
_root_part="3"
_root_size="" # empty value will use remaining disk
# -----------------------------------------------
# SYSTEM LOCALE CONFIGURATION VARIABLES
_console_keymap="dk-latin1"
_console_font="lat2-16"
_console_font_map="8859-10"
_system_locale="en_DK.UTF-8"
_system_tz="Europe/Copenhagen"
_locale_gen_primary="en_DK.UTF-8 UTF-8"
_locale_gen_fallback="en_US.UTF-8 UTF-8"
_systemd_services="NetworkManager systemd-timesyncd systemd-resolved"
# -----------------------------------------------
# PACKAGES
_build_mirror="https://mirror.easyname.at/manjaro"
_base="base mc micro nano"
_kernel_version="6.10"
_kernel_pkg="linux610"
_packages="${_kernel_pkg} ${_kernel_pkg}-headers btrfs-progs efibootmgr git gptfdisk linux-firmware networkmanager plymouth polkit sbctl sudo wget"
_branch="unstable" # requires plymouth 24.x
# installation mountpoint
_install_mount="/install"

# END CONFIGURATION
# DO NOT CHANGE BELOW UNLESS YOU KNOW WHY
#################################################
```


The list is for a bare minimalist system with network - a **Proof of Concept** - and you will have to finalize it to get a fully functional system - see https://forum.manjaro.org/t/root-tip-how-to-do-a-manual-manjaro-installation/12507#recreate-the-live-iso-edition-36 for an idea of to proceed.

## Using TPM

If you are willing to you can use TPM and one time passwords to unlock your encrypted system.

@openminded has provided some guidance in https://forum.manjaro.org/t/howto-using-secure-boot-and-tpm2-to-unlock-luks-partition-on-boot/101626?u=linux-aarhus


[User:ZachHilman/Installation_-_Btrfs_%2B_LUKS2_%2B_Secure_Boot]:https://wiki.archlinux.org/title/User:ZachHilman/Installation_-_Btrfs_%2B_LUKS2_%2B_Secure_Boot

[wonky]: https://codeberg.org/wonky/secure-boot-setup
